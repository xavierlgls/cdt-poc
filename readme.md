# Comment exécuter le projet

1. télécharger les sources de ce répertoire
1. extraire l'archive .zip
1. copier le chemin d'accès de `index.html`*
1. rentrer ce chemin d'accès dans la barre d'URL de votre navigateur web favori (cf liste de compatibilité)

---

**dans l'onglet 'accueil' de l'explorateur windows il y a un bouton 'copier le chemin d'accès', il faudra ensuite veiller à retirer les guillemets au début et à la fin dans la barre d'URL*

# Liste des navigateurs compatibles

*les versions utilisées sont celles du 9/02/2020*

## Compatibles
- google chrome
- microsoft edge
- mozilla firefox
- opera

## Non compatibles
- internet explorer

## Compatibilité inconnue
- safari

# Conditions de bon fonctionnement

Le navigateur doit être connecté à internet pour que le projet puisse s'exécuter correctement