var db_localCenters = [
    {
        users: [
            { lat: 48.12999650994402, lng: -1.626957846745101, name: "Clinique St Laurent", type: "solar-plant" },
            { lat: 48.134804818603655, lng: -1.6305576057552764, type: "house" },
            { lat: 48.13001441215155, lng: -1.6356159080061565, type: "house" },
            { lat: 48.12822416050719, lng: -1.6319835358989578, type: "house" },
            { lat: 48.12654842842367, lng: -1.6293021990460678, name: "CSP auto", type: "company" },
            { lat: 48.13383102029799, lng: -1.6230570281580374, name: "Orange Business Service", type: "company" }
        ],
        manager: "manager #5"
    },
    {
        users: [
            { lat: 48.092527845402735, lng: -1.6203980189058155, name: "Salle Robert Allain", type: "solar-plant" },
            { lat: 48.08814200066847, lng: -1.6214592646617891, name: "Intermarché", type: "solar-plant" },
            { lat: 48.091223597321004, lng: -1.622682492679886, type: "house" },
            { lat: 48.09472779330512, lng: -1.6169530719958593, type: "house" },
            { lat: 48.092782241585205, lng: -1.6149841217514127, type: "house" },
            { lat: 48.08817066753472, lng: -1.6161806616863263, type: "company" }
        ],
        manager: "manager #4"
    },
    {
        users: [
            { lat: 48.0449946032317, lng: -1.6680536995883923, name: "", type: "solar-plant" }
        ],
        manager: "manager #1"
    },
    {
        users: [
            { lat: 48.1761026314364, lng: -1.6504054343052268, name: "Collège François Truffaut", type: "solar-plant" }
        ],
        manager: "manager #2"
    },
    {
        users: [
            { lat: 48.15387608503126, lng: -1.6927734872981847, name: "COSEC", type: "solar-plant" }
        ],
        manager: "manager #3"
    }
];

var db_localCenters = [];

var db_volunteers = [
    {
        lat: 48.11960859689784,
        lng: -1.6105896426986234,
        name: "volontaire #1",
        type: "house"
    },
    {
        lat: 48.125409758759105,
        lng: -1.6068106847471823,
        name: "volontaire #2",
        type: "house"
    },
    {
        lat: 48.12418512358775,
        lng: -1.6143237064696518,
        name: "volontaire #3",
        type: "solar-plant"
    },
    {
        lat: 48.12642310438243,
        lng: -1.616046604838384,
        name: "volontaire #4",
        type: "house"
    },
    {
        lat: 48.11601298009387,
        lng: -1.6324968270751607,
        name: "volontaire #5",
        type: "house"
    },
    {
        lat: 48.13259942540426,
        lng: -1.66020018712588,
        name: "volontaire #6",
        type: "company"
    },
    {
        lat: 48.15366135796619,
        lng: -1.7211631176372213,
        name: "volontaire #7",
        type: "house"
    },
    {
        lat: 48.04262754825027,
        lng: -1.711196180157477,
        name: "volontaire #8",
        type: "house"
    },
    {
        lat: 48.10949438777014,
        lng: -1.7144198875048437,
        name: "volontaire #9",
        type: "company"
    },
    {
        lat: 48.10287466329326,
        lng: -1.7290141420370777,
        name: "volontaire #10",
        type: "house"
    },
    {
        lat: 48.064543751561295,
        lng: -1.6257897494021491,
        name: "volontaire #11",
        type: "company"
    },
    {
        lat: 48.136079320834966,
        lng: -1.5346211200522888,
        name: "volontaire #12",
        type: "house"
    },
    {
        lat: 48.10559716402152,
        lng: -1.5174514088378956,
        name: "volontaire #13",
        type: "house"
    }
];

var db_volunteers = [];